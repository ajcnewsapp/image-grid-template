#Image grid template for producers
Template which builds a grid like [this](http://www.myajc.com/sports/baseball/meet-2015-atlanta-braves/) based on a Google spreadsheet

####Instructions
1. Copy and fill out [this](https://docs.google.com/spreadsheets/d/1mEBRbM_FFk98AOuohhCbNUG4x9vNxQktYIOxxjkK-Lc/edit?usp=sharing) Google spreadsheet
2. Publish the spreadsheet 
3. Click "Share" in upper right and set to "Anyone with the link can view". Copy the share URL and append the it to the template URL (i.e. template.html?publishedSpreadsheetURL). Remove `edit?usp=sharing` from the end of the URL - if that works, you're done! If not, try step 4
4. As of 4/30/19, Google did something to the (old) API tabletop uses and isn't working. If that's still the case, use the `imgGridTemplate_notabletop.html` file as your template instead. Download your Google Sheet as a .csv, convert it to json (you could use [this](https://www.csvjson.com/csv2json) tool), and save the file into `tmp_data` with a `.json` file extension. Append the file name (without `.json`) to your template url, i.e. `imgGridTemplate_notabletop.html?prison_sheet`

More detailed instructions with example [here](https://docs.google.com/document/d/10-57PmlEMkEken9bK05fE1Upsf64HbyApapZm32RR4E/edit) written for producers